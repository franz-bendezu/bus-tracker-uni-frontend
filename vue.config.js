module.exports = {
  pwa: {
    name: "Bus Tracker UNI",
    themeColor: "#711610",
    msTileColor: "#711610",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "#711610",
    // configure the workbox plugin
    manifestOptions: {
      background_color: '#ffffff',
      version: 1.5,
      gcm_sender_id: "103953800507"
    }
  },
  devServer: {
    proxy: {
      '/__/': {
        target: 'https://bustrackuni.web.app',
        secure: true,
        pathRewrite: {
          '^/__/': '/__/',
        },
      },
    },
  },

  productionSourceMap: true,

  "transpileDependencies": [
    "vuetify"
  ]
}
