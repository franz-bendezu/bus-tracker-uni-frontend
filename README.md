# Bus Tracker UNI 

## Project setup
```
npm install
```
## Configuration
### Create file env.local
```
VUE_APP_FIREBASE_API_KEY = 
VUE_APP_FIREBASE_AUTH_DOMAIN = 
VUE_APP_FIREBASE_PROJECT_ID = 
VUE_APP_FIREBASE_MEASUREMENT_ID = 
VUE_APP_FIREBASE_MESSAGING_SENDER_ID = 
VUE_APP_FIREBASE_APP_ID = 
VUE_APP_PUBLIC_VAPID_KEY = 
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
