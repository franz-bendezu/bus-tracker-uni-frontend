//config/firebase-firestore.js
import app from "./firebase";
import  "firebase/firestore"
const db = app.firestore();

db.enablePersistence({synchronizeTabs:true}).then(() => {
    console.log("Woohoo! Multi-Tab Persistence!");
})
    .catch(function(err) {
        console.error(err)
        if (err.code == 'failed-precondition') {
            // Multiple tabs open, persistence can only be enabled
            // in one tab at a a time.
            // ...
        } else if (err.code == 'unimplemented') {
            // The current browser does not support all of the
            // features required to enable persistence
            // ...
        }
    });

export default db