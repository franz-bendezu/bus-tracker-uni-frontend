
import router from "@/router";
import firebaseAuth from "@/config/firebase-auth";
import firebase from "firebase";

const auth = {
    namespaced: true,
    state: {
        user: null,
        from: null,
        to: null,
        loading: false,
        error: null,
    },
    getters: {
        isAuthenticated(state) {
            return !!state.user
        },
        isAnonymous(state) {
            return state.user.isAnonymous||false
        }
    },
    mutations: {
        setLoading(state, loading) {
            state.loading = loading
        },
        setUser(state, user) {
            state.user = user
        },
        setError(state, error) {
            state.error = error
        },
        setFrom(state, from) {
            state.from = from
        } ,
        setTo(state, to) {
            state.to = to
        }
    },
    actions: {
        fetchUser({ commit }, user) {
            commit("setUser", user);
        },
        async signInWithEmailAndPassword({commit}, {username, password}) {
            const credential = await firebaseAuth.signInWithEmailAndPassword(username, password);
            commit("setUser", credential.user);
            if (!credential.user.isAnonymous) {
                await router.push({name: 'Admin'})
            }
        },
        async signInAnonymously({commit, getters}) {
            if (!getters.isAuthenticated) {
                    commit("setLoading", true)
                // User is signed out.
                try {
                    const user = await firebaseAuth.signInAnonymously()
                    commit("setUser", user)
                } catch (e) {
                    commit("setError", e)
                } finally {
                    commit("setLoading", false)
                }
            }
        },
        async checkAuthentication({commit}) {
            commit("setLoading", true)
            try {
                const user = await firebaseAuth.currentUser
                commit("setUser", user)
            } catch (e) {
                commit("setError", e)
            } finally {
                commit("setLoading", false)
            }

        }
    }
}
export default auth
