import firebaseAuth from "@/config/firebase-auth";
import router from "@/router";
import db from "@/config/firestore";
import firebase from "firebase";

const routes = {
    namespaced: true,
    state: {
        routes: [],
    },
    getters: {
    },
    mutations: {
        setRoutes(state, routes) {
            state.routes = routes
        },
        addRoute(state, route) {
            state.route.push(route)
        }
    },
    actions: {
        async addRoute({commit},data) {
            try {
                const docRef = await   db.collection("routes").add(data)
                console.log("Document written with ID: ", docRef.id);
            }catch (e) {
                console.error("Error adding document: ", e);
            }
        },
        async updateRoute({commit}, {id,data}) {
           await db.collection("routes").doc(id).update(data);
        },
        async fetchRoutes({commit}) {
            const routes = []
            const querySnapshot = await db.collection("routes").get()
            querySnapshot.forEach(
                doc => {
                    routes.push({id: doc.id, ...doc.data()})
                }
            )
            commit("setRoutes", routes);
        },
    }
}
export default routes
