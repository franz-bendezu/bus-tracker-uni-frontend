import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "@/store"
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import(/* webpackChunkName: "settings" */ '@/views/Home.vue')
    },
    {
        path: '/admin',
        component: () => import('@/views/Admin.vue'),
        beforeEnter: (to, from, next) => {
            if (store.getters["auth/isAuthenticated"] ||to.name==='AdminLogin') {
                next()
            }else {
                store.commit("auth/setTo",to.name)
                next({ name: 'AdminLogin'})
            }
        },
        children:[
            {
                name: 'Admin',
                path: '',
                component: () => import( '@/views/admin/Index.vue')
            },
            {

                name: 'AdminLogin',
                path: 'login',
                component: () => import( '@/views/admin/Login.vue'),
                beforeEnter: (to, from, next) => {
                    if (store.getters["auth/isAuthenticated"]) {
                        next({ name: 'Admin'})
                    }else {
                        next()
                    }
                },
            },
            {
                name: 'Busses',
                path: 'busses',
                component: () => import( '@/views/admin/Busses.vue'),
            },
            {
                name: 'BusId',
                path: 'routes/:id',
                component: () => import( '@/views/admin/busses/BusId')
            },
            {
                name: 'Drivers',
                path: 'drivers',
                component: () => import( '@/views/admin/Drivers.vue')
            },
            {
                name: 'Routes',
                path: 'routes',
                component: () => import( '@/views/admin/Routes.vue')
            },
            {
                name: 'Schedules',
                path: 'schedules',
                component: () => import( '@/views/admin/Schedules.vue')
            }
        ]
    },
    {
        path: '/settings',
        name: 'Settings',
        component: () => import(/* webpackChunkName: "settings" */ '@/views/Settings.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
router.beforeEach(async (to, from, next) => {
    await store.dispatch("auth/checkAuthentication")
    next()
})


export default router
