export default {
    mounted() {
        if(localStorage.key("darkMode")) {
            let darkMode = localStorage.getItem("darkMode")
            this.$vuetify.theme.dark=JSON.parse(darkMode)
        }
    }
}
