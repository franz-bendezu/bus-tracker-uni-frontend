// custom service-worker.js

if (workbox) {
    // adjust log level for displaying workbox logs
    workbox.setConfig({
        debug: true
    });
    workbox.routing.registerRoute(
        new RegExp('^https://b.tile.openstreetmap.org/'),
        new workbox.strategies.CacheFirst({
            cacheName: 'image-cache',
            plugins: [
                new workbox.cacheableResponse.Plugin({
                    statuses: [0, 200],
                })
            ]
        })
    );
    // apply precaching. In the built version, the precacheManifest will
    // be imported using importScripts (as is workbox itself) and we can
    // precache this. This is all we need for precaching
    workbox.precaching.precacheAndRoute(self.__precacheManifest);

    // Make sure to return a specific response for all navigation requests.
    // Since we have a SPA here, this should be index.html always.
    // https://stackoverflow.com/questions/49963982/vue-router-history-mode-with-pwa-in-offline-mode
    workbox.routing.registerNavigationRoute('/index.html')



}

