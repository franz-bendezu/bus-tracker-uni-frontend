// The Firebase Admin SDK to access Cloud Firestore.
const admin = require('firebase-admin');
admin.initializeApp();

const functions = require('firebase-functions');
const base64 = require('base-64');
const utf8 = require('utf8');
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest(async (req, resp) => {

    const bussId = req.query.bus || 'RTzdX1UQyPkvWGs7iNfB';
    if (req.method === 'POST') {
        const database = admin.database();
        const ref = database.ref("/routes/"+bussId+"/currentPosition");
        // Grab the text parametes
        const body = await req.body;
        // Push the new message into Cloud Firestore using the Firebase Admin SDK.
        const encoded = body.payload_raw
        console.log(encoded)
        const bytes  = base64.decode(encoded);
        const text  = utf8.decode(bytes);
        console.log(text)
        const data = JSON.parse(text);
        console.log(data)
        await ref.set({
          ...data,
          updatedAt: database.ServerValue.TIMESTAMP
        });
        const location =  new admin.firestore.GeoPoint(data.latitude,data.longitude);
        const firestore = admin.firestore();
        await firestore.collection('busses').doc(bussId).set({
            currentLocation: location,
            updatedAt:  Date.now()
        });
        await firestore.collection('busses').doc(bussId).
        collection('historyInformation').add({
            location: location,
            updatedAt:  firestore.FieldValue.serverTimestamp()
        });

        resp.json({result: `Correct update location`});
      }
    else {
        resp.json({result: 'correct'});
    }



 });
